
/*
1 Создайте массив styles с элементами «Джаз» и «Блюз».
2 Добавьте «Рок-н-ролл» в конец.
3 Замените значение в середине на «Классика». Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
4 Удалите первый элемент массива и покажите его.
5 Вставьте «Рэп» и «Регги» в начало массива.

*/
//1
document.write('Домашня робота :' + "<br>" + "<hr>")
var styles = [' Джаз', ' Блюз']
document.write(styles + "<br>")
document.write('' + "<hr>")

//2
document.write('Додання елементу в кінець :' + "<br>")
styles.push(' Рок-н-рол');
document.write(styles + "<br>" + "<hr>")

//3
document.write('Заміна середнього елементу :' + "<br>")
styles.splice(1, 1, 'Класика')
document.write(styles + "<br>" + "<hr>")

//4
document.write('Видалення першого елементу:' + "<br>")
styles.shift
document.write(styles + "<br>" + "<hr>")

//5
document.write('Додання елементів на початок :' + "<br>")
styles.unshift(' Рэп', ' Регги')
document.write(styles + "<br>" + "<hr>")


