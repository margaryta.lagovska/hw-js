// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
// Додати до класу метод для виведення імені та прізвища 

class User {
    constructor() {
        this.name = "";
        this.surname = "";
    }
    show() {
        this.User = document.createElement("div");
        document.body.append(this.User);
        this.User.style.border = "1px solid #000000";
        this.User.style.width = "250px";
        this.User.textContent = `Ім'я: ${this.name} | Прізвище: ${this.surname}`;
    }
}
const user = new User();
user.name = "name";
user.surname = "surname";
user.show()

// Створи список, що складається з 4 аркушів. 
// Використовуючи js зверніся до 2 li і з використанням навігації по DOM дай 1 елементу синій фон, а 3 червоний 

const ul = document.createElement('ul');
const li1 = document.createElement('li');
const li2 = document.createElement('li');
const li3 = document.createElement('li');
const li4 = document.createElement('li');

document.body.append(ul);
ul.append(li1, li2,li3,li4);

const e = document.getElementsByTagName('li')[1];
e.previousElementSibling.style.color ='blue';
e.nextElementSibling.style.color = 'red';


// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
// При наведенні мишки виведіть текст координати, де знаходиться курсор мишки 

const div = document.createElement("div");
document.body.append(div);
div.style.width = "400px";
div.style.height = "400px";
div.style.border = "1px solid #000000";
div.style.textAlign = "center";

div.addEventListener("mousemove", (e) => {
    div.innerHTML = `Координата X: ${e.clientX} | Координата Y: ${e.offsetY}`;
});

// Створи кнопки 1,2,3,4 
// при натисканні на кнопку виведи інформацію про те, яка кнопка була натиснута 

const div2 = document.createElement('div');
const button1 = document.createElement('button');
const button2 = document.createElement('button');
const button3 = document.createElement('button');
const button4 = document.createElement('button');

document.body.append(div2);
div2.append(button1, button2, button3, button4);
div2.style.marginTop = '50px'
button1.innerHTML = "button 1"
button2.innerHTML = "button 2"
button3.innerHTML = "button 3"
button4.innerHTML = "button 4"
button1.style.marginRight = "20px"
button2.style.marginRight = "20px"
button3.style.marginRight = "20px"
button4.style.marginRight = "20px"

const inf = document.createElement('span')
div2.after(inf)
div2.addEventListener('click', (e) => {
    inf.innerText = (`You pressed ${e.target.innerHTML} `)
    alert(inf.innerText)
    console.log(inf)
})

// Створи дiв і зроби так, щоб при наведенні змінював своє положення на сторінці 

const div3 = document.createElement ("div")
document.body.append(div3)
div3.style.width = "100px";
div3.style.height = "100px";
div3.style.backgroundColor = "#ffecef";
div3.style.border = "2px solid #a566ad"
div3.style.marginTop = "20px"
div3.style.position = "relative"
div3.style.left = "20px"
div3.style.transition = "left 2s"

const mouse = (e) => {
    e.target.style.left = "100px"
}
 
div3.addEventListener("mouseover", mouse)

div3.onmouseout = function () {
    div4.style.left = "20px"
}
// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби його фоном body 
const inputColor = document.createElement("input");
const ColorBtn = document.createElement("button");
div3.after(inputColor);
inputColor.after(ColorBtn)
inputColor.setAttribute("type", "color");
ColorBtn.innerText = "Change color"
ColorBtn.style.marginLeft = "10px"
inputColor.style.marginTop = "20px"

ColorBtn.addEventListener("click", (e) => {
let userChoice = inputColor.value;
document.body.style.backgroundColor = userChoice;
})

// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут виводь їх в консоль 

const inputLog = document.createElement("input");
document.body.append(inputLog)
inputLog.setAttribute("placeholder", "Login");
inputLog.style.display = "block";
inputLog.style.marginTop = "20px"
inputLog.style.marginLeft = "10px"

inputLog.addEventListener("input", () => {
    console.log(inputLog.value)
})

// Створіть поле для введення даних у полі введення даних виведіть текст під полем 

const inputLog2 = document.createElement("input");
document.body.append(inputLog2)
inputLog2.setAttribute("placeholder", "Alternative Login");
inputLog2.style.marginTop = "20px"
inputLog2.style.display = "block";
inputLog2.style.marginLeft = "10px"
const text = document.createElement("span");
text.style.marginLeft = "10px"

inputLog2.after(text);

inputLog2.addEventListener("change", () => {
    text.append(`${inputLog2.value} \n`);
    console.log(text)
})
