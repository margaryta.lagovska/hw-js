/*
 Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
 Создать в объекте вложенный объект - "Приложение". 
 Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
 Создать методы для заполнения и отображения документа.
 */


var doc = {
    header: "Заголовок",
    body: "Тiло",
    footer: "Футер",
    date: "Дата",
    app: {
        header: " ",
        body: " ",
        footer: " ",
        date: " "
    },
    fill: function() {
        doc.app.header = prompt("Введіть значення для header");
        doc.app.body = prompt("Введіть значення для body");
        doc.app.footer = prompt("Введіть значення для footer");
        doc.app.date = prompt("Введіть значення для date");
    },
    show: function() {
        document.getElementById("doc").innerHTML = `Ваш документ: <br> header: ${this.header} 
         <br> body: ${this.body}  <br> footer: ${this.footer} <br> date: ${this.date}`;
        document.getElementById("app").innerHTML = `Ваші значення у додатку: 
        <br> ${Object.values(doc.app).join("<br>")}`;   
}
};
doc.fill();
doc.show();



