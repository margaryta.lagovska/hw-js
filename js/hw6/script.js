/*  
Розробіть функцію-конструктор, яка буде створювати об'єкт Human 
Створіть масив об'єктів і реалізуйте функцію
яка буде сортувати елементи масиву за значенням властивості Age за зростанням або за спаданням.
*/

function Human(age, name) {
    this.age = age;
    this.name = name;
}
var a = [new Human(21, "Ira"), new Human(22, "Vika"), new Human(20, "Anya"), new Human(25, "Olena")];
console.log(a.sort((a, b) => a.age - b.age));
var a = [new Human(21, "Ira"), new Human(22, "Vika"), new Human(20, "Anya"), new Human(25, "Olena")];
console.log(a.sort((a, b) => b.age - a.age));

/*
Розробіть функцію-конструктор, яка буде створювати об'єкт Human
Додайте на свій розсуд властивості і методи в цей об'єкт.
Подумайте, які методи і властивості слід зробити рівня екземпляра, а які рівня функції-конструктора.
*/

function HumanObj(name, age, town) {
    this.name = name;
    this.age = age;
    this.town = town;
};

let human_Ira = new HumanObj(" Ira ", " 21 ", " Lviv");
let human_Vika = new HumanObj("Vika ", " 22 ", " Kiyv");
let human_Anya = new HumanObj("Anya ", " 20 ", " Odesa");
let human_Olena = new HumanObj("Olena ", " 25 ", " Ternopil");


HumanObj.prototype.show = function () {
    let info_Ira = Object.values(human_Ira);
    let info_Vika = Object.values(human_Vika);
    let info_Anya = Object.values(human_Anya);
    let info_Olena = Object.values(human_Olena);

    document.write(`${info_Ira.join(" , ")}; </br>`);
    document.write(`${info_Vika.join(" , ")}; </br>`);
    document.write(`${info_Anya.join(" , ")}; </br>`);
    document.write(`${info_Olena.join(" , ")}; </br>`);
};

let people = new HumanObj();
people.show();



