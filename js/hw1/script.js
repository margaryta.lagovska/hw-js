//1
var x = 6;
var y = 14;
var z = 4;
t = x += y - x++ * z;
var result = t;
document.write('y - x++ * z += ' + t + '<br>')
document.write('Пояснення: х++ постінкремент залишається без змін; 6 * 4 = 24; 14 - 24 = -10; 6 + -10 = -4; '+ '<br>')

//2
var x = 6;
var y = 14;
var z = 4;
z = --x - y * 5;
var result = z;
document.write('--x - y * 5 = ' + z + '<br>')
document.write('Пояснення: --x предекремент матиме значення 5 (зменш на 1); 14 * 5 = 70; 5 - 70 = -65;'+ '<br>')

//3
var x = 6;
var y = 14;
var z = 4;
t = y /= x + 5 % z;
var result = t;
document.write('x + 5%z /= ' + t + "<br>")
document.write('Пояснення: % - модуль числа, залишок 1; 6 + 1 = 7; 14/7 = 2;'+ '<br>')

//4
var x = 6;
var y = 14;
var z = 4;
t = z - x++ + y * 5;
var result = t;
document.write('z - x++ + y*5 = '+ t +"<br>")
document.write('Пояснення: x++ постінкремент залишається 6; 14 * 5 = 70; 4 - 6 = -2; -2 + 70 = 68; ' + '<br>')
//5
var x = 6;
var y = 14;
var z = 4;
x = y - x++ * z;
var result = x;
document.write('y - x++ *z = '+ x + "<br>")
document.write('Пояснення: x++ постінкремент залишається 6 * 4 = 24; 14 - 24 = -10;')

